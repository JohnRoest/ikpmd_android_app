package com.johnroest.ikpmd.Infrastructure;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Date;

/**
 * Created by john on 12/28/2017.
 */

public class Session {

    private final String ACCESS_TOKEN_KEY = "accesstoken";
    private final String REFRESH_TOKEN_KEY = "refreshtoken";
    private final String DURATION_KEY = "duration";
    private final String USER_EMAIL_KEY = "userEmail";
    private final String IKPMD_REALM = "IKPMD";
    private final String LAST_STORED_TIME = "lastStoredTime";

    private static Session singletonSession;

    private Session(){}

    public static Session getInstance()
    {
        if(singletonSession == null)
        {
            singletonSession = new Session();
        }

        return singletonSession;
    }

    public void storeAccessToken(Context context,
                                 String accessToken)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences(IKPMD_REALM,
                Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(ACCESS_TOKEN_KEY, accessToken);

        editor.apply();

        this.updateLastStoredTime(context);
    }

    public void storeUserEmail(Context context,
                               String userEmail)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences(IKPMD_REALM,
                Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(USER_EMAIL_KEY, userEmail);

        editor.apply();
    }

    public String getUserEmail(Context context)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences(IKPMD_REALM,
                Context.MODE_PRIVATE);

        /// Return empty string if the access token is not found
        return sharedPreferences.getString(USER_EMAIL_KEY, "");
    }

    public String getAccessToken(Context context)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences(IKPMD_REALM,
                Context.MODE_PRIVATE);

        /// Return empty string if the access token is not found
        return sharedPreferences.getString(ACCESS_TOKEN_KEY, "");
    }

    private void updateLastStoredTime(Context context)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences(IKPMD_REALM,
                Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPreferences.edit();

        Date date = new Date(System.currentTimeMillis());

        long millis = date.getTime();

        editor.putLong(LAST_STORED_TIME, millis);

        editor.apply();
    }

    public void storeRefreshToken(Context context,
                                  String refreshToken)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences(IKPMD_REALM,
                Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(REFRESH_TOKEN_KEY, refreshToken);

        editor.apply();
    }

    public String getRefreshToken(Context context)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences(IKPMD_REALM,
                Context.MODE_PRIVATE);

        /// Return empty string if the refresh token is not found
        return sharedPreferences.getString(REFRESH_TOKEN_KEY, "");
    }

    public void storeDuration(Context context,
                              Integer duration)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences(IKPMD_REALM,
                Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPreferences.edit();

        System.out.println(duration == null);
        editor.putInt(DURATION_KEY, duration);

        editor.apply();
    }

    public boolean isExpired(Context context)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences(IKPMD_REALM,
                Context.MODE_PRIVATE);

        /// Retrieve the duration (how long access token is valid).
        Integer duration = sharedPreferences.getInt(DURATION_KEY, 1);
        /// Retrieve the last time (a date converted to millis) the access token was updated.
        Long lastUpdatedTimeInMillis = sharedPreferences.getLong(LAST_STORED_TIME, 1);

        /// The duration in millis.
        Long durationInMillis = duration * 1000L;

        /// Turn the current date into millis so we can calculate with it.
        Date currentDate = new Date(System.currentTimeMillis());
        Long currentTimeInMillis = currentDate.getTime();

        return currentTimeInMillis - lastUpdatedTimeInMillis >= durationInMillis;
    }

}
