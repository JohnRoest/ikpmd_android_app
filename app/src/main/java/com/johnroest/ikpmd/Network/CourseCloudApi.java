package com.johnroest.ikpmd.Network;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.johnroest.ikpmd.Domain.Course;
import com.johnroest.ikpmd.Infrastructure.Session;
import com.johnroest.ikpmd.Utils.Errors;
import com.johnroest.ikpmd.Utils.EventEmitter;
import com.johnroest.ikpmd.Utils.OnCourseAddedEvent;
import com.johnroest.ikpmd.Utils.OnCourseDeletedEvent;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by johnroest on 24/11/2017.
 */

public class CourseCloudApi extends CloudApi {

    private final static String GET_ALL_COURSES_API_URL = BASE_URL+"course";
    private final static String CREATE_COURSE_API_URL = BASE_URL+"course";
    private final static String GET_COURSE_BY_ID_API_URL = BASE_URL+"course/{id}";
    private final static String DELETE_COURSE_API_URL = BASE_URL+"course/{id}";

    private OkHttpClient httpClient = new OkHttpClient();

    public Observable<List<Course>> all(final Context context)
    {

        return Observable
                .create(new ObservableOnSubscribe<List<Course>>() {
                    @Override
                    public void subscribe(final ObservableEmitter<List<Course>> emitter) throws Exception {

                        Request request = new Request.Builder()
                                .url(GET_ALL_COURSES_API_URL)
                                .addHeader(AUTHORIZATION_KEY,
                                        "Bearer " + Session.getInstance().getAccessToken(context))
                                .build();

                        httpClient.newCall(request).enqueue(new Callback() {
                            @Override
                            public void onFailure(Call call, IOException e)
                            {
                                emitter.onError(e);
                                emitter.onComplete();
                            }

                            @Override
                            public void onResponse(Call call,
                                                   Response response) throws IOException
                            {
                                String responseBody = response.body().string();

                                if(response.code() == 200)
                                {

                                    //emitter.onNext();
                                    Type listType = new TypeToken<List<Course>>(){}.getType();
                                    List<Course> courses = new Gson().fromJson(responseBody,
                                            listType);

                                    emitter.onNext(courses);
                                    emitter.onComplete();
                                }
                                else
                                {
                                    Type type = new TypeToken<Map<String, String>>(){}.getType();
                                    Map<String, String> responseBodyHashMap = new Gson().
                                            fromJson(responseBody, type);

                                    System.out.println(responseBodyHashMap.get(Errors.ERROR.toString()));
                                    if(responseBodyHashMap.containsKey(Errors.ERROR.toString())
                                            && responseBodyHashMap.get(Errors.ERROR.toString())
                                            .equals(Errors.INVALID_ACCESS_TOKEN.toString()))
                                    {
                                        Session.getInstance().storeDuration(context, 0);
                                    }

                                    emitter.onError(new Throwable());
                                    emitter.onComplete();
                                }
                            }
                        });
                    }
                });

    }

    public Observable<Course> byId(final Context context,
                                         final Integer courseId)
    {

        return Observable
                .create(new ObservableOnSubscribe<Course>() {
                    @Override
                    public void subscribe(final ObservableEmitter<Course> emitter) throws Exception {

                        Request request = new Request.Builder()
                                .url(GET_COURSE_BY_ID_API_URL.replace("{id}", courseId.toString()))
                                .addHeader(AUTHORIZATION_KEY,
                                        "Bearer " + Session.getInstance().getAccessToken(context))
                                .build();

                        httpClient.newCall(request).enqueue(new Callback() {
                            @Override
                            public void onFailure(Call call, IOException e)
                            {
                                emitter.onError(e);
                                emitter.onComplete();
                            }

                            @Override
                            public void onResponse(Call call, Response response) throws
                                    IOException
                            {
                                String responseBody = response.body().string();

                                if(response.code() == 200)
                                {

                                    //emitter.onNext();
                                    Type listType = new TypeToken<Course>(){}.getType();
                                    Course course = new Gson().fromJson(responseBody,
                                            listType);

                                    emitter.onNext(course);
                                    emitter.onComplete();
                                }
                                else
                                {
                                    emitter.onError(new Throwable());
                                    emitter.onComplete();
                                }
                            }
                        });
                    }
                });
    }


    public Observable<Course> create(final Context context,
                                     final Course course)
    {
        return Observable
                .create(new ObservableOnSubscribe<Course>() {
                    @Override
                    public void subscribe(final ObservableEmitter<Course> emitter) throws Exception {

                        String courseJson = new Gson().toJson(course);
                        RequestBody requestBody = RequestBody.create(CONTENT_TYPE_JSON,
                                courseJson);

                        Request request = new Request.Builder()
                                .url(CREATE_COURSE_API_URL)
                                .addHeader(AUTHORIZATION_KEY,
                                        "Bearer " + Session.getInstance()
                                                .getAccessToken(context))
                                .post(requestBody)
                                .build();

                        httpClient.newCall(request).enqueue(new Callback()
                        {
                            @Override
                            public void onFailure(Call call, IOException e)
                            {
                                emitter.onError(e);
                                emitter.onComplete();
                            }

                            @Override
                            public void onResponse(Call call, Response response) throws
                                    IOException
                            {
                                String responseBody = response.body().string();

                                if(response.code() == 200)
                                {
                                    Type courseType = new TypeToken<Course>(){}.getType();
                                    Course course = new Gson().fromJson(responseBody,
                                            courseType);

                                    EventEmitter.getInstance().courseAdded(new OnCourseAddedEvent(course));

                                    emitter.onNext(course);
                                    emitter.onComplete();
                                }
                                else
                                {
                                    emitter.onError(new Throwable());
                                    emitter.onComplete();
                                }
                            }
                        });
                    }
                });
    }

    public Observable<Boolean> delete(final Context context,
                                      final Course course)
    {
        return Observable
                .create(new ObservableOnSubscribe<Boolean>() {
                    @Override
                    public void subscribe(final ObservableEmitter<Boolean> emitter) throws Exception {

                        final String courseId = course.getId().toString();

                        Request request = new Request.Builder()
                                .url(DELETE_COURSE_API_URL.replace("{id}", courseId))
                                .addHeader(AUTHORIZATION_KEY,
                                        "Bearer " + Session.getInstance()
                                                           .getAccessToken(context))
                                .delete()
                                .build();

                        httpClient.newCall(request).enqueue(new Callback()
                        {
                            @Override
                            public void onFailure(Call call, IOException e)
                            {
                                emitter.onError(e);
                                emitter.onComplete();
                            }

                            @Override
                            public void onResponse(Call call, Response response) throws
                                                                                 IOException
                            {
                                if(response.code() == 200)
                                {
                                    EventEmitter.getInstance().courseDeleted(new OnCourseDeletedEvent());

                                    emitter.onNext(true);
                                    emitter.onComplete();
                                }
                                else
                                {
                                    emitter.onError(new Throwable());
                                    emitter.onComplete();
                                }
                            }
                        });
                    }
                });
    }

}
