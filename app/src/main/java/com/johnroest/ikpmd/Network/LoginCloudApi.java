package com.johnroest.ikpmd.Network;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.johnroest.ikpmd.Domain.User;
import com.johnroest.ikpmd.Infrastructure.Session;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by john on 12/28/2017.
 */

public class LoginCloudApi extends CloudApi {

    private final static String SIGN_IN_URL = BASE_URL+"oauth/token";
    private final static String SIGN_UP_URL = BASE_URL+"sign-up";
    private OkHttpClient httpClient = new OkHttpClient();

    public Observable<Boolean> login(final Context context,
                                     final User user)
    {

        return Observable
                .create(new ObservableOnSubscribe<Boolean>() {
                    @Override
                    public void subscribe(final ObservableEmitter<Boolean> emitter) throws Exception {

                        RequestBody formBody = new FormBody.Builder()
                                .add("username", user.getEmail())
                                .add("password", user.getPassword())
                                .add("grant_type", "password")
                                .build();


                        Request request = new Request.Builder()
                                .url(SIGN_IN_URL)
                                .addHeader(AUTHORIZATION_KEY,
                                            CLIENT_AUTHORIZATION_BASE64)
                                .post(formBody)
                                .build();


                        httpClient.newCall(request).enqueue(new Callback() {
                            @Override
                            public void onFailure(Call call, IOException e)
                            {
                                emitter.onError(e);
                                emitter.onComplete();
                            }

                            @Override
                            public void onResponse(Call call, Response response) throws
                                    IOException
                            {
                                String responseBody = response.body().string();
                                if(response.code() == 200)
                                {
                                    Type type = new TypeToken<Map<String, String>>(){}.getType();

                                    Map<String,String> sessionMap = new Gson()
                                            .fromJson(responseBody, type);

                                    Session session = Session.getInstance();


                                    session.storeAccessToken(context, sessionMap.get("access_token"));
                                    session.storeRefreshToken(context, sessionMap.get("refresh_token"));
                                    session.storeDuration(context, Integer.parseInt(sessionMap.get("expires_in")));

                                    emitter.onNext(true);
                                    emitter.onComplete();
                                }
                                else
                                {
                                    emitter.onError(new Throwable());
                                    emitter.onComplete();
                                }
                            }
                        });
                    }
                });

    }

    public Observable<User> signUp(final Context context,
                                     final User user)
    {

        return Observable
                .create(new ObservableOnSubscribe<User>() {
                    @Override
                    public void subscribe(final ObservableEmitter<User> emitter) throws Exception {

                        String courseJson = new Gson().toJson(user);
                        RequestBody requestBody = RequestBody.create(CONTENT_TYPE_JSON,
                                courseJson);

                        Request request = new Request.Builder()
                                .url(SIGN_UP_URL)
                                .addHeader(AUTHORIZATION_KEY,
                                        "Bearer " + Session.getInstance()
                                                .getAccessToken(context))
                                .post(requestBody)
                                .build();

                        httpClient.newCall(request).enqueue(new Callback() {
                            @Override
                            public void onFailure(Call call, IOException e)
                            {
                                emitter.onError(e);
                                emitter.onComplete();
                            }

                            @Override
                            public void onResponse(Call call, Response response) throws
                                    IOException
                            {
                                String responseBody = response.body().string();
                                if(response.code() == 200)
                                {
                                    Type type = new TypeToken<User>(){}.getType();

                                    User user = new Gson()
                                            .fromJson(responseBody, type);

                                    emitter.onNext(user);
                                    emitter.onComplete();
                                }
                                else
                                {
                                    emitter.onError(new Throwable());
                                    emitter.onComplete();
                                }
                            }
                        });
                    }
                });

    }
}
