package com.johnroest.ikpmd.Network;

import okhttp3.MediaType;

/**
 * Created by johnroest on 24/11/2017.
 */

class CloudApi {

    final static String DEFAULT_PORT = "8080";
    final static String BASE_URL = "http://10.0.2.2:"+DEFAULT_PORT+"/";
    final static String AUTHORIZATION_KEY = "Authorization";
    final static String CLIENT_AUTHORIZATION_BASE64 = "Basic SUtQTUQtQ0xJRU5UOnNlY3JldA==";
    final static MediaType CONTENT_TYPE_JSON = MediaType.parse("application/json; charset=utf-8");

}
