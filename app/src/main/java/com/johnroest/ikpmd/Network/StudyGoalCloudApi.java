package com.johnroest.ikpmd.Network;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.johnroest.ikpmd.Domain.StudyGoal;
import com.johnroest.ikpmd.Infrastructure.Session;
import com.johnroest.ikpmd.Utils.EventEmitter;
import com.johnroest.ikpmd.Utils.OnStudyGoalAddedEvent;

import java.io.IOException;
import java.lang.reflect.Type;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by john on 12/29/2017.
 */

public class StudyGoalCloudApi extends CloudApi {

    private final static String STUDYGOAL_BY_ID = BASE_URL+"studygoal/{id}";
    private final static String CREATE_STUDY_GOAL_URL= BASE_URL+"studygoal";
    private final static String DELETE_STUDY_GOAL_URL= BASE_URL+"studygoal/{id}";

    private OkHttpClient httpClient = new OkHttpClient();

    public Observable<StudyGoal> update(final Context context,
                                        final StudyGoal studyGoal)
    {
        return Observable
                .create(new ObservableOnSubscribe<StudyGoal>()
                {
                    @Override
                    public void subscribe(final ObservableEmitter<StudyGoal> emitter) throws Exception
                    {

                        String studyGoalJson = new Gson().toJson(studyGoal);

                        RequestBody requestBody = RequestBody.create(CONTENT_TYPE_JSON,
                                studyGoalJson);

                        Request request = new Request.Builder()
                                .url(STUDYGOAL_BY_ID.replace("{id}", studyGoal.getId().toString()))
                                .addHeader(AUTHORIZATION_KEY,
                                        "Bearer " + Session.getInstance().getAccessToken(context))
                                .put(requestBody)
                                .build();

                        httpClient.newCall(request).enqueue(new Callback()
                        {
                            @Override
                            public void onFailure(Call call, IOException e)
                            {
                                emitter.onError(e);
                                emitter.onComplete();
                            }

                            @Override
                            public void onResponse(Call call, Response response) throws
                                    IOException
                            {
                                String responseBody = response.body().string();
                                if (response.code() == 200)
                                {
                                    Type type = new TypeToken<StudyGoal>() {
                                    }.getType();

                                    StudyGoal studyGoal = new Gson()
                                            .fromJson(responseBody, type);

                                    emitter.onNext(studyGoal);
                                    emitter.onComplete();
                                }
                                    else
                                {
                                    emitter.onError(new Throwable());
                                    emitter.onComplete();
                                }
                            }
                        });
                    }
                });
    }

    public Observable<StudyGoal> create(final Context context,
                                        final StudyGoal studyGoal)
    {
        return Observable
                .create(new ObservableOnSubscribe<StudyGoal>()
                {
                    @Override
                    public void subscribe(final ObservableEmitter<StudyGoal> emitter) throws Exception
                    {
                        String studyGoalJson = new Gson().toJson(studyGoal, StudyGoal.class);

                        RequestBody requestBody = RequestBody.create(CONTENT_TYPE_JSON,
                                studyGoalJson);

                        Request request = new Request.Builder()
                                .url(CREATE_STUDY_GOAL_URL)
                                .addHeader(AUTHORIZATION_KEY,
                                        "Bearer " + Session.getInstance().getAccessToken(context))
                                .post(requestBody)
                                .build();

                        httpClient.newCall(request).enqueue(new Callback()
                        {
                            @Override
                            public void onFailure(Call call, IOException e)
                            {
                                emitter.onError(e);
                                emitter.onComplete();
                            }

                            @Override
                            public void onResponse(Call call, Response response) throws
                                    IOException
                            {
                                String responseBody = response.body().string();
                                if (response.code() == 200)
                                {
                                    Type type = new TypeToken<StudyGoal>() {}.getType();

                                    StudyGoal studyGoal = new Gson()
                                            .fromJson(responseBody, type);

                                    EventEmitter.getInstance().studyGoalAdded(new OnStudyGoalAddedEvent(studyGoal));

                                    emitter.onNext(studyGoal);
                                    emitter.onComplete();
                                }
                                else
                                {
                                    emitter.onError(new Throwable());
                                    emitter.onComplete();
                                }
                            }
                        });
                    }
                });
    }

    public Observable<Boolean> delete(final Context context,
                                        final StudyGoal studyGoal)
    {
        return Observable
                .create(new ObservableOnSubscribe<Boolean>()
                {
                    @Override
                    public void subscribe(final ObservableEmitter<Boolean> emitter) throws Exception
                    {

                        final String studyGoalId = studyGoal.getId().toString();
                        Request request = new Request.Builder()
                                .url(DELETE_STUDY_GOAL_URL.replace("{id}", studyGoalId))
                                .addHeader(AUTHORIZATION_KEY,
                                        "Bearer " + Session.getInstance().getAccessToken(context))
                                .delete()
                                .build();

                        httpClient.newCall(request).enqueue(new Callback()
                        {
                            @Override
                            public void onFailure(Call call, IOException e)
                            {
                                emitter.onError(e);
                                emitter.onComplete();
                            }

                            @Override
                            public void onResponse(Call call, Response response) throws
                                    IOException
                            {
                                String responseBody = response.body().string();
                                if (response.code() == 200)
                                {
                                    emitter.onNext(true);
                                    emitter.onComplete();
                                }
                                else
                                {
                                    emitter.onError(new Throwable());
                                    emitter.onComplete();
                                }
                            }
                        });
                    }
                });
    }

}
