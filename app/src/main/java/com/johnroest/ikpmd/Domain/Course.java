package com.johnroest.ikpmd.Domain;

import java.util.List;

/**
 * Created by johnroest on 23/11/2017.
 */

public class Course {

    private Integer id;
    private String name;
    private User user;
    private List<StudyGoal> studyGoals;

    public Course(Integer id, String name, List<StudyGoal> studyGoals)
    {
        this.id = id;
        this.name = name;
        this.studyGoals = studyGoals;
    }

    public Course(Integer id, String name, User user, List<StudyGoal> studyGoals) {
        this.id = id;
        this.name = name;
        this.user = user;
        this.studyGoals = studyGoals;
    }

    public Course(){}

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public List<StudyGoal> getStudyGoals()
    {
        return studyGoals;
    }

    public void setStudyGoals(List<StudyGoal> studyGoals)
    {
        this.studyGoals = studyGoals;
    }

    public Integer getTotalHourOfGoals()
    {
        int totalAmountOfHours = 0;

        if(!studyGoals.isEmpty()) {
            for (StudyGoal studyGoal : studyGoals) {
                totalAmountOfHours += studyGoal.getHoursToComplete();
            }
        }
        return totalAmountOfHours;
    }

    @Override
    public String toString() {
        return "Course{\n\t" +
                "\nid=" + id +
                ", \nname='" + name + '\'' +
                ", \nuser=" + user +
                ", \nstudyGoals=" + studyGoals +
                '}';
    }
}
