package com.johnroest.ikpmd.Domain;

/**
 * Created by johnroest on 23/11/2017.
 */

public class StudyGoal {

    private Integer id;
    private String description;
    private Integer hoursToComplete;
    private Course course;
    private User user;
    private Boolean isDone;
    private Integer courseId;

    public StudyGoal(Integer id,
                     String description,
                     Integer hoursToComplete,
                     Boolean isDone,
                     Integer courseId)
    {
        this.id = id;
        this.description = description;
        this.hoursToComplete = hoursToComplete;
        this.isDone = isDone;
        this.courseId = courseId;
    }

    public StudyGoal(Integer id,
                     String description,
                     Integer hoursToComplete,
                     Course course,
                     User user,
                     Boolean isDone,
                     Integer courseId)
    {
        this.id = id;
        this.description = description;
        this.hoursToComplete = hoursToComplete;
        this.course = course;
        this.user = user;
        this.isDone = isDone;
        this.courseId = courseId;
    }

    public StudyGoal(){}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getHoursToComplete() {
        return hoursToComplete;
    }

    public void setHoursToComplete(Integer hoursToComplete) {
        this.hoursToComplete = hoursToComplete;
    }

    public Boolean isDone() {
        return isDone;
    }

    public void setIsDone(Boolean done) {
        isDone = done;
    }

    public Integer getCourseId()
    {
        return courseId;
    }

    public void setCourseId(Integer courseId)
    {
        this.courseId = courseId;
    }

    @Override
    public String toString() {
        return "StudyGoal{\n\t" +
                "\ndescription='" + description + '\'' +
                ", \nhoursToComplete=" + hoursToComplete +
                ", \ncourse=" + course +
                ", \nuser=" + user +
                ", \nisDone=" + isDone +
                ", \ncourseId=" + courseId +
                '}';
    }
}
