package com.johnroest.ikpmd.Repository.Course;

import android.content.Context;

import com.johnroest.ikpmd.Domain.User;
import com.johnroest.ikpmd.Network.LoginCloudApi;

import io.reactivex.Observable;

/**
 * Created by johnroest on 22-01-18.
 */

public class ApiLoginRepository implements LoginRepository {

    private LoginCloudApi loginCloudApi;

    public ApiLoginRepository(LoginCloudApi loginCloudApi)
    {
        this.loginCloudApi = loginCloudApi;
    }


    @Override
    public Observable<Boolean> login(Context context, User user)
    {
        return this.loginCloudApi.login(context, user);
    }

}
