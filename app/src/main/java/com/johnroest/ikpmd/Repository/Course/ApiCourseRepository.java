package com.johnroest.ikpmd.Repository.Course;

import android.content.Context;

import com.johnroest.ikpmd.Domain.Course;
import com.johnroest.ikpmd.Network.CourseCloudApi;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by johnroest on 24/11/2017.
 */

public class ApiCourseRepository implements CourseRepository {

    private CourseCloudApi courseCloudApi;

    public ApiCourseRepository(CourseCloudApi courseCloudApi)
    {
        this.courseCloudApi = courseCloudApi;
    }


    @Override
    public Observable<List<Course>> all(Context context)
    {
        return this.courseCloudApi.all(context);
    }

    @Override
    public Observable<Course> byId(Context context, Course course)
    {
        return this.courseCloudApi.byId(context, course.getId());
    }

    @Override
    public Observable<Boolean> delete(Context context, Course course)
    {
        return this.courseCloudApi.delete(context, course);
    }

    @Override
    public Observable<Course> create(Context context, Course course)
    {
        return this.courseCloudApi.create(context, course);
    }

}
