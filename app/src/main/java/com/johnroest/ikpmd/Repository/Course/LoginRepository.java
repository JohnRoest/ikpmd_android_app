package com.johnroest.ikpmd.Repository.Course;

import android.content.Context;

import com.johnroest.ikpmd.Domain.User;

import io.reactivex.Observable;

/**
 * Created by johnroest on 22-01-18.
 */

public interface LoginRepository {

    Observable<Boolean> login(Context context, User user);

}
