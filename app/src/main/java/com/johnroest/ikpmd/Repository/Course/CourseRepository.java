package com.johnroest.ikpmd.Repository.Course;

import android.content.Context;

import com.johnroest.ikpmd.Domain.Course;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by johnroest on 24/11/2017.
 */

public interface CourseRepository {

    Observable<List<Course>> all(Context context);
    Observable<Course> byId(Context context, Course course);
    Observable<Boolean> delete(Context context, Course course);
    Observable<Course> create(Context context, Course course);

}
