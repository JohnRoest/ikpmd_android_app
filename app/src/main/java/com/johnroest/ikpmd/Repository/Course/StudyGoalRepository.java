package com.johnroest.ikpmd.Repository.Course;

import android.content.Context;

import com.johnroest.ikpmd.Domain.StudyGoal;

import io.reactivex.Observable;

/**
 * Created by john on 1/17/2018.
 */

public interface StudyGoalRepository {

    Observable<StudyGoal> create(Context context, StudyGoal studyGoal);
    Observable<StudyGoal> update(Context context, StudyGoal studyGoal);
    Observable<Boolean> delete(Context context, StudyGoal studyGoal);
    Observable<StudyGoal> byId(Context context, Integer id);

}
