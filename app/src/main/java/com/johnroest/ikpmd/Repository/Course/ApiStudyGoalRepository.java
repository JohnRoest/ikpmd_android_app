package com.johnroest.ikpmd.Repository.Course;

import android.content.Context;

import com.johnroest.ikpmd.Domain.StudyGoal;
import com.johnroest.ikpmd.Network.StudyGoalCloudApi;

import io.reactivex.Observable;

/**
 * Created by john on 1/17/2018.
 */

public class ApiStudyGoalRepository implements StudyGoalRepository {

    private StudyGoalCloudApi studyGoalCloudApi;

    public ApiStudyGoalRepository(StudyGoalCloudApi studyGoalCloudApi)
    {
        this.studyGoalCloudApi = studyGoalCloudApi;
    }

    @Override
    public Observable<StudyGoal> create(Context context, StudyGoal studyGoal)
    {
        return this.studyGoalCloudApi.create(context, studyGoal);
    }

    @Override
    public Observable<StudyGoal> update(Context context, StudyGoal studyGoal)
    {
        return this.studyGoalCloudApi.update(context, studyGoal);
    }

    @Override
    public Observable<Boolean> delete(Context context, StudyGoal studyGoal)
    {
        return this.studyGoalCloudApi.delete(context, studyGoal);
    }

    @Override
    public Observable<StudyGoal> byId(Context context, Integer id)
    {
        return null;
    }

}
