package com.johnroest.ikpmd.Utils;

import com.johnroest.ikpmd.Domain.Course;

/**
 * Created by john on 12/31/2017.
 */

public class OnCourseAddedEvent {

    private Course course;

    public OnCourseAddedEvent(Course course) {
        this.course = course;
    }

    public Course getCourse() {
        return course;
    }
}
