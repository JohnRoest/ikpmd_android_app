package com.johnroest.ikpmd.Utils;

import com.johnroest.ikpmd.Domain.StudyGoal;

/**
 * Created by john on 12/31/2017.
 */

public interface OnStudyGoalAddedEventListener {

    void studyGoalAdded(StudyGoal studyGoal);
}
