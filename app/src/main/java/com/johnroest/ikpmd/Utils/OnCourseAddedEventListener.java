package com.johnroest.ikpmd.Utils;

import com.johnroest.ikpmd.Domain.Course;

/**
 * Created by john on 12/31/2017.
 */

public interface OnCourseAddedEventListener {

    void courseAdded(Course course);

}
