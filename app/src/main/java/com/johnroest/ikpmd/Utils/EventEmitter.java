package com.johnroest.ikpmd.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by john on 12/31/2017.
 */

public class EventEmitter {

    private static final EventEmitter eventEmitter = new EventEmitter();

    private List<OnCourseAddedEventListener> onCourseAddedEventListenerList = new ArrayList<>();

    private List<OnStudyGoalAddedEventListener> onStudyGoalAddedEventListenerList = new ArrayList<>();

    private List<OnCourseDeletedEventListener> onCourseDeletedEventListenerList = new ArrayList<>();

    public static EventEmitter getInstance() {
        return eventEmitter;
    }

    private EventEmitter() {
    }

    /// Course

    public void addOnCourseAddedEventListener(OnCourseAddedEventListener onCourseAddedEventListener)
    {
        this.onCourseAddedEventListenerList.add(onCourseAddedEventListener);
    }

    public void removeOnCourseAddedEventListener(OnCourseAddedEventListener onCourseAddedEventListener)
    {
        this.onCourseAddedEventListenerList.remove(onCourseAddedEventListener);
    }

    public void courseAdded(OnCourseAddedEvent onCourseAddedEvent)
    {
        for(OnCourseAddedEventListener listener : onCourseAddedEventListenerList)
        {
            listener.courseAdded(onCourseAddedEvent.getCourse());
        }
    }

    /// Study Goal

    public void addOnStudyGoalAddedEventListener(OnStudyGoalAddedEventListener onStudyGoalAddedEventListener)
    {
        this.onStudyGoalAddedEventListenerList.add(onStudyGoalAddedEventListener);
    }

    public void removeOnStudyGoalAddedEventListener(OnStudyGoalAddedEventListener onStudyGoalAddedEventListener)
    {
        this.onStudyGoalAddedEventListenerList.remove(onStudyGoalAddedEventListener);
    }

    public void studyGoalAdded(OnStudyGoalAddedEvent onStudyGoalAddedEvent)
    {
        for(OnStudyGoalAddedEventListener listener : onStudyGoalAddedEventListenerList)
        {
            listener.studyGoalAdded(onStudyGoalAddedEvent.getStudyGoal());
        }
    }

    public void addOnCourseDeletedEventListener(OnCourseDeletedEventListener onCourseDeletedEventListener)
    {
        this.onCourseDeletedEventListenerList.add(onCourseDeletedEventListener);
    }

    public void removeOnCourseDeletedEventListener(OnCourseDeletedEventListener onCourseDeletedEventListener)
    {
        this.onCourseDeletedEventListenerList.remove(onCourseDeletedEventListener);
    }

    public void courseDeleted(OnCourseDeletedEvent onCourseDeletedEvent)
    {
        for(OnCourseDeletedEventListener listener : onCourseDeletedEventListenerList)
        {
            listener.courseDeleted();
        }
    }

}
