package com.johnroest.ikpmd.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.johnroest.ikpmd.Activity.LoginActivity;
import com.johnroest.ikpmd.Infrastructure.Session;

/**
 * Created by john on 12/31/2017.
 */

public class Navigator {

    private final Intent intent;
    private final Context context;
    private final Activity activity;

    public Navigator(Intent intent,
                     Context context,
                     Activity activity)
    {
        this.intent = intent;
        this.context = context;
        this.activity = activity;
    }

    public void show()
    {
        if(!Session.getInstance().isExpired(context))
        {
            context.startActivity(intent);
        }
        else
        {
            Intent intent = new Intent(context,
                    LoginActivity.class);
            context.startActivity(intent);
            activity.finish();
        }
    }
}
