package com.johnroest.ikpmd.Utils;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by johnroest on 26/11/2017.
 */

public class ErrorHandler {

    public ErrorHandler(Context context, String message)
    {
        this.showErrorMessage(context, message);
    }

    private void showErrorMessage(Context context, String message)
    {
        Toast.makeText(context,
                message,
                Toast.LENGTH_SHORT)
             .show();
    }
}
