package com.johnroest.ikpmd.Utils;

/**
 * Created by johnroest on 22-01-18.
 */

public interface OnCourseDeletedEventListener {

    void courseDeleted();

}
