package com.johnroest.ikpmd.Utils;

import com.johnroest.ikpmd.Domain.StudyGoal;

/**
 * Created by john on 12/31/2017.
 */

public class OnStudyGoalAddedEvent {

    private StudyGoal studyGoal;

    public OnStudyGoalAddedEvent(StudyGoal studyGoal) {
        this.studyGoal = studyGoal;
    }

    public StudyGoal getStudyGoal() {
        return studyGoal;
    }
}
