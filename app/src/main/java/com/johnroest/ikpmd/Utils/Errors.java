package com.johnroest.ikpmd.Utils;

/**
 * Created by john on 12/31/2017.
 */

public enum Errors {
    ERROR("error"),
    INVALID_ACCESS_TOKEN("invalid_token");

    private final String text;

    Errors(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
