package com.johnroest.ikpmd.Fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.TextView;

import com.johnroest.ikpmd.Activity.AddCourseActivity;
import com.johnroest.ikpmd.Activity.StudyGoalDetailActivity;
import com.johnroest.ikpmd.Domain.Course;
import com.johnroest.ikpmd.Domain.StudyGoal;
import com.johnroest.ikpmd.Network.CourseCloudApi;
import com.johnroest.ikpmd.R;
import com.johnroest.ikpmd.Repository.Course.ApiCourseRepository;
import com.johnroest.ikpmd.Repository.Course.CourseRepository;
import com.johnroest.ikpmd.Utils.ErrorHandler;
import com.johnroest.ikpmd.Utils.EventEmitter;
import com.johnroest.ikpmd.Utils.Navigator;
import com.johnroest.ikpmd.Utils.OnCourseAddedEventListener;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by johnroest on 23/11/2017.
 */

public class CoursesFragment extends ListFragment implements OnCourseAddedEventListener {

    public static final String COURSES_FRAGMENT_COURSE_ID = "CoursesFragment.Course.Id";

    private List<Course> coursesList = new ArrayList<>();
    private CoursesArrayAdapter coursesArrayAdapter;
    private CourseRepository courseRepository;

    public CoursesFragment()
    {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        EventEmitter.getInstance().addOnCourseAddedEventListener(this);

        this.boot();
    }

    private void boot()
    {
        this.setupRepositories();
        this.setupFloatingActionButton();
        this.getAllCourses();
        this.setupAdapter();
    }

    private void setupRepositories()
    {
        this.courseRepository = new ApiCourseRepository(new CourseCloudApi());
    }

    private void setupFloatingActionButton()
    {
        FloatingActionButton floatingActionButton = getActivity()
                .findViewById(R.id.floating_action_button);

        floatingActionButton.setOnClickListener(new FloatingActionButtonClickListener());
    }

    private void setupAdapter()
    {
        this.coursesArrayAdapter = new CoursesArrayAdapter(
                getActivity(),
                R.id.course_name,
                new ArrayList<String>());

        setListAdapter(coursesArrayAdapter);
    }

    private void getAllCourses()
    {
        this.courseRepository.all(getActivity())
                             .subscribeOn(Schedulers.newThread())
                             .observeOn(AndroidSchedulers.mainThread())
                             .subscribe(new Observer<List<Course>>()
                             {
                                 @Override
                                 public void onNext(List<Course> courses)
                                 {
                                     fillAdapter(courses);
                                 }

                                 @Override
                                 public void onError(Throwable t)
                                 {
                                     t.printStackTrace();
                                 }

                                 @Override
                                 public void onSubscribe(Disposable d)
                                 {

                                 }

                                 @Override
                                 public void onComplete()
                                 {

                                 }
                });
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.courses_fragment,
                container,
                false);
    }

    public void fillAdapter(List<Course> courses)
    {
        coursesList = courses;

        this.coursesArrayAdapter.clear();

        for(Course course : courses)
        {
            if(course.getName() != null)
            {
                this.coursesArrayAdapter.add(course.getName());
            }
        }

        this.coursesArrayAdapter.notifyDataSetChanged();
    }

    @Override
    public void courseAdded(Course course)
    {
        getAllCourses();
    }

    private void deleteCourse(Course course)
    {
        this.courseRepository.delete(getActivity(), course)
                             .subscribeOn(Schedulers.newThread())
                             .observeOn(AndroidSchedulers.mainThread())
                             .subscribe(new Observer<Boolean>()
                             {
                                 @Override
                                 public void onNext(Boolean isDeleted)
                                 {
                                     getAllCourses();
                                 }

                                 @Override
                                 public void onError(Throwable t)
                                 {
                                     new ErrorHandler(getActivity(), t.getMessage());
                                 }

                                 @Override
                                 public void onSubscribe(Disposable d)
                                 {
                                     // do nothing.
                                 }

                                 @Override
                                 public void onComplete()
                                 {
                                     // do nothing.
                                 }
                             });
    }

    class FloatingActionButtonClickListener implements View.OnClickListener
    {
        @Override
        public void onClick(View v)
        {
            Intent intent = new Intent(getActivity(),
                    AddCourseActivity.class);
            Navigator navigator = new Navigator(intent,
                    getActivity(),
                    getActivity());
            navigator.show();
        }
    }

    class CoursesArrayAdapter extends ArrayAdapter<String> {

        CoursesArrayAdapter(
                @NonNull Context context,
                int resource,
                @NonNull List<String> objects)
        {
            super(context,
                    resource,
                    objects);
        }

        @Override
        public View getView(final int position,
                            View convertView,
                            ViewGroup parent)
        {
            // Get the data item for this position
            final Course course = coursesList.get(position);

            if(course != null)
            {
                // Check if an existing view is being reused, otherwise inflate the view
                if (convertView == null)
                {
                    convertView = LayoutInflater.from(getContext()).inflate(R.layout.course_row,
                            parent,
                            false);
                }

                TextView courseNameTextView = convertView.findViewById(R.id.course_name);
                ImageButton deleteCourseImageButton = convertView.findViewById(R.id.trash_can_course);

                courseNameTextView.setText(course.getName());
                deleteCourseImageButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {
                        deleteCourse(course);
                    }
                });

                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {
                        Intent intent = new Intent(getActivity(),
                                StudyGoalDetailActivity.class);
                        intent.putExtra(COURSES_FRAGMENT_COURSE_ID, coursesList.get(position).getId());

                        Navigator navigator = new Navigator(intent, getActivity(), getActivity());
                        navigator.show();
                    }
                });

                return convertView;
            }

            return convertView;
        }
    }
}
