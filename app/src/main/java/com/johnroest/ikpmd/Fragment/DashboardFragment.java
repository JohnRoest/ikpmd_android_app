package com.johnroest.ikpmd.Fragment;


import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.johnroest.ikpmd.Domain.Course;
import com.johnroest.ikpmd.Domain.StudyGoal;
import com.johnroest.ikpmd.Network.CourseCloudApi;
import com.johnroest.ikpmd.R;
import com.johnroest.ikpmd.Repository.Course.ApiCourseRepository;
import com.johnroest.ikpmd.Repository.Course.CourseRepository;
import com.johnroest.ikpmd.Utils.ErrorHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by johnroest on 23/11/2017.
 */

public class DashboardFragment extends Fragment {

    private CourseRepository courseRepository;
    private TextView dataTextView;

    public DashboardFragment()
    {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        this.boot();
        this.getAllCourses();
    }

    private void boot()
    {
        this.setupRepositories();
    }

    private void setupRepositories()
    {
        this.courseRepository = new ApiCourseRepository(new CourseCloudApi());
    }

    public void getAllCourses()
    {
        this.courseRepository.all(getActivity())
                             .subscribeOn(Schedulers.newThread())
                             .observeOn(AndroidSchedulers.mainThread())
                             .subscribe(new Observer<List<Course>>()
                             {
                                 @Override
                                 public void onNext(List<Course> courses)
                                 {
                                     if(!courses.isEmpty())
                                     {
                                         generateVisualisation(courses);
                                     }
                                 }

                                 @Override
                                 public void onError(Throwable t)
                                 {
                                     t.printStackTrace();
                                 }

                                 @Override
                                 public void onSubscribe(Disposable d)
                                 {

                                 }

                                 @Override
                                 public void onComplete()
                                 {

                                 }
                             });
    }

    @Override
    public void onResume()
    {
        super.onResume();

        this.dataTextView = getView().findViewById(R.id.dashboard_text_view);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.dashboard_fragment,
                container,
                false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
    }

    private void generateVisualisation(List<Course> courseList)
    {
        final Integer amountOfHoursDone = this.calculateAmountOfHoursDone(courseList);
        final Integer amountOfHoursToGo = this.calculateAmountOfHoursToGo(courseList);
        final String amountOfCoursesDone = this.calculateAmountOfCoursesDone(courseList);

        String visualisation = "Aantal uren gestudeerd = " + amountOfHoursDone + "\n"
                               + "Aantal uren te gaan = " + amountOfHoursToGo +"\n"
                                + "Aantal afgeronde lessen = " + amountOfCoursesDone;

        this.dataTextView.setText(visualisation);
    }

    private Integer calculateAmountOfHoursDone(List<Course> courseList)
    {
        Integer amountOfHoursDone = 0;

        for(Course course : courseList)
        {
            for(StudyGoal studyGoal : course.getStudyGoals())
            {
                if(studyGoal.isDone())
                {
                    amountOfHoursDone += studyGoal.getHoursToComplete();
                }
            }
        }

        return amountOfHoursDone;
    }

    private Integer calculateAmountOfHoursToGo(List<Course> courseList)
    {
        Integer amountOfHoursToGo = 0;

        for(Course course : courseList)
        {
            if(course != null &&
               course.getStudyGoals() != null &&
               !course.getStudyGoals().isEmpty())
            {
                for (StudyGoal studyGoal : course.getStudyGoals())
                {
                    if (!studyGoal.isDone())
                    {
                        amountOfHoursToGo += studyGoal.getHoursToComplete();
                    }
                }
            }
        }

        return amountOfHoursToGo;
    }

    private String calculateAmountOfCoursesDone(List<Course> courseList)
    {
        Integer totalAmountOfCourses = courseList.size();
        Integer amountOfCompletedCourses;

        List<Course> completedCourses = new ArrayList<>();
        completedCourses.addAll(courseList);

        for(Course course : courseList)
        {
            if(course != null &&
               course.getStudyGoals() != null &&
               !course.getStudyGoals().isEmpty())
            {
                for (StudyGoal studyGoal : course.getStudyGoals())
                {
                    if (!studyGoal.isDone())
                    {
                        completedCourses.remove(course);

                    }
                }
            }
        }

        amountOfCompletedCourses = completedCourses.size();

        return amountOfCompletedCourses.toString() + " / " + totalAmountOfCourses.toString();
    }

}

