package com.johnroest.ikpmd.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.johnroest.ikpmd.Infrastructure.Session;

/**
 * Created by johnroest on 21-01-18.
 */

public class AuthenticatedActivity extends AppCompatActivity {

    @Override
    protected void onCreate(
            @Nullable
                    Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        this.checkIsExpired();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        this.checkIsExpired();
    }

    private void checkIsExpired()
    {
        boolean isExpired = Session.getInstance().isExpired(this);

        if(isExpired)
        {
            Intent intent = new Intent(this,
                    LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }
}
