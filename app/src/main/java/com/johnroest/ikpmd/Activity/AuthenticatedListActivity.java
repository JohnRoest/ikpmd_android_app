package com.johnroest.ikpmd.Activity;

import android.app.ListActivity;
import android.content.Intent;

import com.johnroest.ikpmd.Infrastructure.Session;

/**
 * Created by johnroest on 21-01-18.
 */

public class AuthenticatedListActivity extends ListActivity {

    @Override
    protected void onResume()
    {
        super.onResume();
        this.checkIsExpired();
    }

    private void checkIsExpired()
    {
        boolean isExpired = Session.getInstance().isExpired(this);

        if(isExpired)
        {
            Intent intent = new Intent(this,
                    LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }
}
