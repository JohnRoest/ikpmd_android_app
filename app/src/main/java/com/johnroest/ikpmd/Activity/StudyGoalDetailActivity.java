package com.johnroest.ikpmd.Activity;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.johnroest.ikpmd.Domain.Course;
import com.johnroest.ikpmd.Domain.StudyGoal;
import com.johnroest.ikpmd.Fragment.CoursesFragment;
import com.johnroest.ikpmd.Network.CourseCloudApi;
import com.johnroest.ikpmd.Network.StudyGoalCloudApi;
import com.johnroest.ikpmd.R;
import com.johnroest.ikpmd.Repository.Course.ApiStudyGoalRepository;
import com.johnroest.ikpmd.Repository.Course.StudyGoalRepository;
import com.johnroest.ikpmd.Utils.ErrorHandler;
import com.johnroest.ikpmd.Utils.EventEmitter;
import com.johnroest.ikpmd.Utils.Navigator;
import com.johnroest.ikpmd.Utils.OnStudyGoalAddedEventListener;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by johnroest on 11/12/2017.
 */

public class StudyGoalDetailActivity extends AuthenticatedListActivity implements OnStudyGoalAddedEventListener {

    public static final String STUDY_GOAL_DETAIL_ACTIVITY_COURSE_ID = "Activity.StudyGoalDetailActivity.courseId";

    private GoalArrayAdapter goalArrayAdapter;
    private ListView listView;
    private Course course;
    private Integer courseId;
    private StudyGoalRepository studyGoalRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.study_goals_fragment);

        this.boot();
    }

    private void boot()
    {
        this.setupRepositories();
        this.setupEvents();
        this.getCourse();
        this.setupFloatingActionButton();
        this.setupListView();
        this.setupListAdapter();
    }

    private void setupRepositories()
    {
        this.studyGoalRepository = new ApiStudyGoalRepository(new StudyGoalCloudApi());
    }

    public void setupEvents()
    {
        EventEmitter.getInstance().addOnStudyGoalAddedEventListener(this);
    }

    private void setupListView()
    {
        this.listView = findViewById(android.R.id.list);
        this.listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
    }

    private void getCourse()
    {
        Intent intent = getIntent();
        this.courseId = intent.getIntExtra(CoursesFragment.COURSES_FRAGMENT_COURSE_ID,
                9999);

        getAllStudyGoals();
    }

    private void getAllStudyGoals()
    {
        CourseCloudApi courseCloudApi = new CourseCloudApi();
        courseCloudApi.byId(this, courseId)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Course>()
                {
                    @Override
                    public void onNext(Course apiCourse)
                    {
                        course = apiCourse;
                        goalArrayAdapter.clear();
                        goalArrayAdapter.addAll(getStudyGoalDescriptions(apiCourse));

                        goalArrayAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onError(Throwable t)
                    {
                        t.printStackTrace();
                        new ErrorHandler(getBaseContext(), t.getMessage());
                    }

                    @Override
                    public void onSubscribe(Disposable d)
                    {
                        // do nothing.
                    }

                    @Override
                    public void onComplete()
                    {
                        // do nothing.
                    }
                });
    }

    private void setupFloatingActionButton()
    {
        FloatingActionButton floatingActionButton = findViewById(R.id.done_fab_study_goals);
        floatingActionButton.setOnClickListener(new FloatingActionButtonOnClickListener(this,
                this));
    }

    private void setupListAdapter()
    {
        this.goalArrayAdapter = new GoalArrayAdapter(this,
                R.layout.checked_text_view,
                new ArrayList<String>());
        this.listView.setAdapter(this.goalArrayAdapter);
    }

    private List<String> getStudyGoalDescriptions(Course course)
    {
        List<String> studyGoals = new ArrayList<>();

        if(course != null &&
                !course.getStudyGoals().isEmpty())
        {
            for (StudyGoal studyGoal : course.getStudyGoals())
            {
                studyGoals.add(studyGoal.getDescription());
            }
        }

        return studyGoals;
    }

    @Override
    public void studyGoalAdded(StudyGoal studyGoal)
    {
        this.getAllStudyGoals();
    }

    class GoalArrayAdapter extends ArrayAdapter<String> {

         GoalArrayAdapter(
                 @NonNull Context context,
                int resource,
                @NonNull List<String> objects)
        {
            super(context,
                    resource,
                    objects);
        }

        @Override
        public View getView(final int position,
                            View convertView,
                            ViewGroup parent)
        {
            // Get the data item for this position
            if(course != null)
            {
                StudyGoal studyGoal = course.getStudyGoals().get(position);

                // Check if an existing view is being reused, otherwise inflate the view
                if (convertView == null) {
                    convertView = LayoutInflater.from(getContext()).inflate(R.layout.study_goal_row,
                            parent,
                            false);
                }

                final TextView descriptionTextView = convertView.findViewById(R.id.study_goal_description);
                final TextView amountOfHoursTextView = convertView.findViewById(R.id.study_goal_amount_of_hours);
                final Button isDoneButton = convertView.findViewById(R.id.is_done_button);
                final ImageButton deleteImageButton = convertView.findViewById(R.id.trash_can_study_goal);
                final TextView isDoneTextView = convertView.findViewById(R.id.is_done_text_view);

                String amountOfHours = "Duur (in uren): " + studyGoal.getHoursToComplete().toString();

                descriptionTextView.setText(studyGoal.getDescription());

                amountOfHoursTextView.setText(amountOfHours);

                String isDone = studyGoal.isDone() ? "Klaar" : "Nog te doen.";
                isDoneTextView.setText(isDone);

                isDoneButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {
                        if (course != null)
                        {
                            StudyGoal studyGoal = course.getStudyGoals()
                                                        .get(position);
                            Course tempCourse = new Course();
                            tempCourse.setId(courseId);
                            studyGoal.setCourse(tempCourse);

                            studyGoal.setIsDone(!studyGoal.isDone());

                            studyGoalRepository.update(getContext(),
                                    studyGoal)
                                               .subscribeOn(Schedulers.newThread())
                                               .observeOn(AndroidSchedulers.mainThread())
                                               .subscribe(new Observer<StudyGoal>() {
                                                   @Override
                                                   public void onNext(StudyGoal studyGoal)
                                                   {
                                                       Toast.makeText(
                                                               getContext(),
                                                               "Studiedoel aangepast!",
                                                               Toast.LENGTH_SHORT);

                                                       String isDone = studyGoal.isDone() ? "Klaar" : "Nog te doen.";
                                                       isDoneTextView.setText(isDone);
                                                   }

                                                   @Override
                                                   public void onError(Throwable t)
                                                   {
                                                       t.printStackTrace();
                                                       new ErrorHandler(getContext(),
                                                               t.getMessage());
                                                   }

                                                   @Override
                                                   public void onSubscribe(Disposable d)
                                                   {
                                                       // do nothing.
                                                   }

                                                   @Override
                                                   public void onComplete()
                                                   {
                                                       // do nothing.
                                                   }
                                               });
                        }
                    }
                });



                deleteImageButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {
                        studyGoalRepository.delete(getContext(),
                                course.getStudyGoals().get(position))
                                           .subscribeOn(Schedulers.newThread())
                                           .observeOn(AndroidSchedulers.mainThread())
                                           .subscribe(new Observer<Boolean>()
                                           {
                                               @Override
                                               public void onNext(Boolean bool)
                                               {
                                                   getCourse();
                                               }

                                               @Override
                                               public void onError(Throwable t)
                                               {
                                                   t.printStackTrace();
                                                   new ErrorHandler(getBaseContext(), t.getMessage());
                                               }

                                               @Override
                                               public void onSubscribe(Disposable d)
                                               {
                                                   // do nothing.
                                               }

                                               @Override
                                               public void onComplete()
                                               {
                                                   // do nothing.
                                               }
                                           });;
                    }
                });

                return convertView;
            }

            return convertView;
        }
    }

    class FloatingActionButtonOnClickListener implements View.OnClickListener {

        private final Context context;
        private final Activity activity;

        public FloatingActionButtonOnClickListener(Context context, Activity activity)
        {
            this.context = context;
            this.activity = activity;
        }

        @Override
        public void onClick(View v)
        {
            Intent intent = new Intent(context,
                    AddStudyGoalActivity.class);
            intent.putExtra(STUDY_GOAL_DETAIL_ACTIVITY_COURSE_ID, course.getId());
            Navigator navigator = new Navigator(intent,
                    context,
                    activity);
            navigator.show();
        }
    }

}
