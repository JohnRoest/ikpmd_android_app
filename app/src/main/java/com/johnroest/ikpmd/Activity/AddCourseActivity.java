package com.johnroest.ikpmd.Activity;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.johnroest.ikpmd.Domain.Course;
import com.johnroest.ikpmd.Domain.User;
import com.johnroest.ikpmd.Infrastructure.Session;
import com.johnroest.ikpmd.Network.CourseCloudApi;
import com.johnroest.ikpmd.R;
import com.johnroest.ikpmd.Repository.Course.ApiCourseRepository;
import com.johnroest.ikpmd.Repository.Course.CourseRepository;
import com.johnroest.ikpmd.Utils.ErrorHandler;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class AddCourseActivity extends AuthenticatedActivity
{

    private String courseName;
    private CourseRepository courseRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        this.boot();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
    }

    private void boot()
    {
        setTitle("Toevoegen les");
        this.setupRepositories();
        this.setupContentView();
        this.setupToolBar();
        this.setupEditTextChangedListener();
        this.setupFloatingActionButton();
    }

    private void setupRepositories()
    {
        this.courseRepository = new ApiCourseRepository(new CourseCloudApi());
    }

    private void setupContentView()
    {
        setContentView(R.layout.activity_add_course);
    }

    private void setupToolBar()
    {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    private void setupEditTextChangedListener()
    {
        EditText editText = findViewById(R.id.course_name_edit_text);
        editText.addTextChangedListener(new CourseNameEditTextTextWatcher());
    }

    private void setupFloatingActionButton()
    {
        FloatingActionButton floatingActionButton = findViewById(R.id.done_fab);
        floatingActionButton.setOnClickListener(new FloatingActionButtonOnClickListener(this));
    }

    class CourseNameEditTextTextWatcher implements TextWatcher
    {
        @Override
        public void beforeTextChanged(CharSequence s,
                                      int start,
                                      int count,
                                      int after)
        {
            // do nothing
        }

        @Override
        public void onTextChanged(CharSequence s,
                                  int start,
                                  int before,
                                  int count)
        {
            if(!s.toString().isEmpty())
            {
                courseName = s.toString();
            }
        }

        @Override
        public void afterTextChanged(Editable s)
        {
            /// do nothing
        }
    }

    class FloatingActionButtonOnClickListener implements View.OnClickListener
    {

        private final Context context;

        public FloatingActionButtonOnClickListener(Context context)
        {
            this.context = context;
        }

        @Override
        public void onClick(View v)
        {
            if (!courseName.isEmpty())
            {
                User user = new User(Session.getInstance().getUserEmail(context));
                Course course = new Course();
                course.setName(courseName);
                course.setUser(user);

                courseRepository.create(
                        context,
                        course)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Observer<Course>()
                        {
                            @Override
                            public void onNext(Course course)
                            {
                                courseSuccesfullyAdded();
                            }

                            @Override
                            public void onError(Throwable t)
                            {
                                t.printStackTrace();
                                new ErrorHandler(getBaseContext(), t.getMessage());
                            }

                            @Override
                            public void onSubscribe(Disposable d)
                            {
                                // do nothing.
                            }

                            @Override
                            public void onComplete()
                            {
                                // do nothing.
                            }
                        });
            }
        }
    }

    private void courseSuccesfullyAdded()
    {
        finish();
        Toast.makeText(this, "Les aangemaakt!", Toast.LENGTH_SHORT).show();
    }

}

