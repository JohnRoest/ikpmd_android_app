package com.johnroest.ikpmd.Activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.johnroest.ikpmd.Domain.User;
import com.johnroest.ikpmd.Infrastructure.Session;
import com.johnroest.ikpmd.Network.LoginCloudApi;
import com.johnroest.ikpmd.R;
import com.johnroest.ikpmd.Repository.Course.ApiLoginRepository;
import com.johnroest.ikpmd.Repository.Course.LoginRepository;
import com.johnroest.ikpmd.Utils.ErrorHandler;
import com.johnroest.ikpmd.Utils.Navigator;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;

    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;
    private UserSignUpTask mSignUpTask = null;

    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;

    private LoginRepository loginRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if(!Session.getInstance().isExpired(this))
        {
            showMainActivity();
        }

        this.loginRepository = new ApiLoginRepository(new LoginCloudApi());
        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL)
                {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        Button mSignUpButton = (Button) findViewById(R.id.email_sign_up_button);
        mSignUpButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                signUp();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            mAuthTask = new UserLoginTask(email, password, this);
            mAuthTask.login();
        }
    }

    public void signUp()
    {
        if (mSignUpTask != null) {
            return;
        }

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password))
        {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email))
        {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        }
        else if (!isEmailValid(email))
        {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel)
        {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else
            {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            mSignUpTask = new UserSignUpTask(email, password, this);
            mSignUpTask.signUp();
        }
    }

    private boolean isEmailValid(String email)
    {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password)
    {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show)
    {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2)
        {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        }
        else
        {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /**
     * Represents an asynchronous login task used to authenticate
     * the user.
     */
    public class UserLoginTask {

        private final String mEmail;
        private final String mPassword;
        private final Context context;

        UserLoginTask(String email, String password, Context context)
        {
            this.mEmail = email;
            this.mPassword = password;
            this.context = context;
        }

        public void login()
        {
            final User user = new User(mEmail, mPassword);

            loginRepository.login(context, user)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Boolean>()
                    {
                        @Override
                        public void onNext(Boolean loggedIn)
                        {
                            storeUserEmail(user.getEmail());
                            Toast.makeText(context,
                                    "Ingelogd!",
                                    Toast.LENGTH_SHORT);
                            showMainActivity();
                        }

                        @Override
                        public void onError(Throwable t) {
                            new ErrorHandler(context,
                                    getString(R.string.something_went_wrong));
                            mAuthTask = null;
                            showProgress(false);
                        }

                        @Override
                        public void onSubscribe(Disposable d) {
                            showProgress(true);
                        }

                        @Override
                        public void onComplete()
                        {
                            showProgress(false);
                        }
                    });
        }

        private void storeUserEmail(String userEmail)
        {
            Session.getInstance().storeUserEmail(context, userEmail);
        }
    }

    private void showMainActivity()
    {
        Intent intent = new Intent(this,
                MainActivity.class);

        Navigator navigator = new Navigator(intent,
                this,
                this);
        navigator.show();
    }


    /**
     * Represents an asynchronous registration task used to authenticate
     * the user.
     */
    public class UserSignUpTask {

        private final String mEmail;
        private final String mPassword;
        private final Context context;

        UserSignUpTask(String email, String password, Context context)
        {
            this.mEmail = email;
            this.mPassword = password;
            this.context = context;
        }

        public void signUp()
        {
            final User user = new User(mEmail, mPassword);
            LoginCloudApi loginCloudApi = new LoginCloudApi();
            loginCloudApi.signUp(context, user)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<User>()
                    {
                        @Override
                        public void onNext(User user)
                        {
                            Toast.makeText(context,
                                    R.string.succesfully_logged_in,
                                    Toast.LENGTH_SHORT);

                            successfullyRegistered();
                        }

                        @Override
                        public void onError(Throwable t)
                        {
                            new ErrorHandler(context,
                                    getString(R.string.something_went_wrong));
                            mSignUpTask = null;
                            showProgress(false);
                        }

                        @Override
                        public void onSubscribe(Disposable d) {
                            showProgress(true);
                        }

                        @Override
                        public void onComplete()
                        {
                            showProgress(false);
                        }
                    });
        }

        private void successfullyRegistered()
        {
            attemptLogin();
        }
    }

}
