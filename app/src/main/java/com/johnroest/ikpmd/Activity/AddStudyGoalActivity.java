package com.johnroest.ikpmd.Activity;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.johnroest.ikpmd.Domain.Course;
import com.johnroest.ikpmd.Domain.StudyGoal;
import com.johnroest.ikpmd.Domain.User;
import com.johnroest.ikpmd.Infrastructure.Session;
import com.johnroest.ikpmd.Network.StudyGoalCloudApi;
import com.johnroest.ikpmd.R;
import com.johnroest.ikpmd.Repository.Course.ApiStudyGoalRepository;
import com.johnroest.ikpmd.Repository.Course.StudyGoalRepository;
import com.johnroest.ikpmd.Utils.ErrorHandler;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class AddStudyGoalActivity extends AuthenticatedActivity {

    private String description;
    private Integer hoursToComplete;
    private Boolean isDone = false;
    private Integer courseId;
    private StudyGoalRepository studyGoalRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_add_study_goal);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.courseId = getIntent().getIntExtra(StudyGoalDetailActivity.STUDY_GOAL_DETAIL_ACTIVITY_COURSE_ID,
                9999);
        this.boot();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
    }

    private void boot()
    {
        this.setupRepositories();
        this.setupFloatingActionButton();
        this.setupDescriptionEditText();
        this.setupHoursToCompleteEditText();
        this.setupIsDoneCheckBox();
    }

    private void setupRepositories()
    {
        this.studyGoalRepository = new ApiStudyGoalRepository(new StudyGoalCloudApi());
    }

    private void setupFloatingActionButton()
    {
        FloatingActionButton floatingActionButton = findViewById(R.id.done_fab_study_goals);
        floatingActionButton.setOnClickListener(new FloatingActionButtonOnClickListener(this));
    }

    private void setupDescriptionEditText()
    {
        EditText descriptionEditText = findViewById(R.id.study_goal_description_edit_text);
        descriptionEditText.addTextChangedListener(new DescriptionEditTextTextWatcher());
    }

    private void setupHoursToCompleteEditText()
    {
        EditText hoursToCompleteEditText = findViewById(R.id.study_goal_hours_edit_text);
        hoursToCompleteEditText.addTextChangedListener(new HoursToCompleteEditTextTextWatcher());
    }

    private void setupIsDoneCheckBox()
    {
        CheckBox isDoneCheckBox = findViewById(R.id.study_goal_is_completed_checkbox);
        isDoneCheckBox.setOnCheckedChangeListener(new IsDoneOnCheckBoxChangeListener());
    }

    class FloatingActionButtonOnClickListener implements View.OnClickListener
    {
        private final Context context;

        public FloatingActionButtonOnClickListener(Context context)
        {
            this.context = context;
        }

        @Override
        public void onClick(View v)
        {
            if (description != null &&
                hoursToComplete != null)
            {
                addStudyGoal();
            }
            else
            {
                new ErrorHandler(context, "Niet alle velden zijn ingevuld!");
            }
        }
    }

    private void addStudyGoal()
    {
        final StudyGoal studyGoal = new StudyGoal();
        studyGoal.setDescription(this.description);
        studyGoal.setHoursToComplete(this.hoursToComplete);
        studyGoal.setIsDone(this.isDone);

        Course course = new Course();
        course.setId(this.courseId);
        studyGoal.setCourseId(this.courseId);

        User user = new User();
        user.setEmail(Session.getInstance().getUserEmail(this));

        studyGoal.setCourse(course);
        studyGoal.setUser(user);

        this.studyGoalRepository.create(this,
                studyGoal)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<StudyGoal>() {
                    @Override
                    public void onNext(StudyGoal studyGoal)
                    {
                        successfullyAddedStudyGoal();
                    }

                    @Override
                    public void onError(Throwable t)
                    {
                        t.printStackTrace();
                        new ErrorHandler(getBaseContext(), t.getMessage());
                    }

                    @Override
                    public void onSubscribe(Disposable d)
                    {
                        // do nothing.
                    }

                    @Override
                    public void onComplete()
                    {
                        // do nothing.
                    }
                });
       }

    private void successfullyAddedStudyGoal()
    {
        Toast.makeText(this,
                "Toegevoegd!",
                Toast.LENGTH_SHORT).show();
        finish();
    }

    class DescriptionEditTextTextWatcher implements TextWatcher
    {
        @Override
        public void beforeTextChanged(CharSequence s,
                                      int start,
                                      int count,
                                      int after)
        {
            // do nothing
        }

        @Override
        public void onTextChanged(CharSequence s,
                                  int start,
                                  int before,
                                  int count)
        {
            if(!s.toString().isEmpty())
            {
                description = s.toString();
            }
        }

        @Override
        public void afterTextChanged(Editable s)
        {
            /// do nothing
        }
    }

    class HoursToCompleteEditTextTextWatcher implements TextWatcher
    {
        @Override
        public void beforeTextChanged(CharSequence s,
                                      int start,
                                      int count,
                                      int after)
        {
            // do nothing
        }

        @Override
        public void onTextChanged(CharSequence s,
                                  int start,
                                  int before,
                                  int count)
        {
            if(!s.toString().isEmpty())
            {
                hoursToComplete = Integer.parseInt(s.toString());
            }
        }

        @Override
        public void afterTextChanged(Editable s)
        {
            /// do nothing
        }
    }

    class IsDoneOnCheckBoxChangeListener implements CompoundButton.OnCheckedChangeListener
    {
        @Override
        public void onCheckedChanged(CompoundButton buttonView,
                                     boolean isChecked)
        {
            isDone = isChecked;
        }
    }

}
